<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Hola extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('calendar');
        $this->load->library('table');
    }

    public function view()
    {
        $data['main_title'] = 'Biblioteca';
        $data['title2'] = 'Hola';
        $this->load->view('templates/header', $data);
        $this->load->view('hola_view');
        $this->load->view('templates/footer');
    }
}
