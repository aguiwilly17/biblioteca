<?php

class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
                $this->load->library('upload');
        }

        public function index()
        {
                $data['main_title'] = 'Biblioteca';
                $data['title2'] = 'Subir imágenes';
                $this->load->view('templates/header', $data);
                $this->load->view('upload/upload_form', array('error' => ' ' ));
                $this->load->view('templates/footer');
        }

        public function do_upload()
        {
                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                        $data['main_title'] = 'Biblioteca';
                        $data['title2'] = 'Subir imágenes';
                        $this->load->view('templates/header', $data);
                        $this->load->view('upload/upload_form', $error);
                        $this->load->view('templates/footer');
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $data['main_title'] = 'Biblioteca';
                        $data['title2'] = 'Subir imágenes';
                        $this->load->view('templates/header', $data);
                        $this->load->view('upload/upload_success', $data);
                        $this->load->view('templates/footer');
                }
        }
}
?>